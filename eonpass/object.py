 ########::'#######::'##::: ##:'########:::::'###:::::'######:::'######::
 ##.....::'##.... ##: ###:: ##: ##.... ##:::'## ##:::'##... ##:'##... ##:
 ##::::::: ##:::: ##: ####: ##: ##:::: ##::'##:. ##:: ##:::..:: ##:::..::
 ######::: ##:::: ##: ## ## ##: ########::'##:::. ##:. ######::. ######::
 ##...:::: ##:::: ##: ##. ####: ##.....::: #########::..... ##::..... ##:
 ##::::::: ##:::: ##: ##:. ###: ##:::::::: ##.... ##:'##::: ##:'##::: ##:
 ########:. #######:: ##::. ##: ##:::::::: ##:::: ##:. ######::. ######::
#.......:::.......:::..::::..::..:::::::::..:::::..:::......::::......:::
# EONPASS protocol - object class
#    MIT License - Cryptomice 2018

import os
import hashlib
import configparser
from ecdsa import VerifyingKey, SigningKey, SECP256k1

class Object:

    EONclass_ = "object"
    EONversion_ = "0.0.1"

    def __init__(self, configFile):
        self.EONclass = ""
        self.EONversion = ""
        self.filename = ""
        self.content = ""
        self.signer = ""
        self.signature = ""

        config = configparser.RawConfigParser()
        config.read(configFile)
        self.identity = config.get('EONPASS', 'identity')
        self.doc_path = config.get('EONPASS', 'documents')
        self.key_path = config.get('EONPASS', 'keys')

        os.makedirs(self.doc_path, exist_ok=True)
        os.makedirs(self.key_path, exist_ok=True)

    def print(self):
        print("--- Object - "+ self.hash())
        print(self.message(True))
        print("----------------------------------------")

    def sign(self):
        privKey = self.key_path+"/"+self.signer+"_private.pem"
        sk = SigningKey.from_pem(open(privKey).read())
        message = self.message(False)
        signature = sk.sign(message.encode('utf_8')).hex()
        return signature

    def checkSig(self):
        pubKey = self.key_path+"/"+self.signer+"_public.pem"
        vk = VerifyingKey.from_pem(open(pubKey).read())
        message = self.message(False)
        sig = bytes.fromhex(self.signature)
        try:
            res = vk.verify(sig, message.encode('utf_8'))
        except:
            print(self.filename+" has bad signature!")
            res = False
        return res

    def message(self, signature):
        message = "EONPASS Protocol: "+self.EONclass+": "+self.EONversion+"\n"
        message += "Content: "+self.content+"\n"
        if (signature):
            message += "Signature: "+self.signer+":"+self.signature
        return message

    def hash(self):
        hash = hashlib.sha256(self.message(True).encode('utf-8')).hexdigest()
        return hash

    def load(self, filename):
        self.filename = filename
        file = open(self.doc_path+"/"+self.filename,"r",encoding='utf-8')
        for line in file:
            element = line.split(':')
            if element[0]=='EONPASS Protocol':
                self.EONclass = element[1].strip()
                self.EONversion = element[2].strip()
            elif element[0]=="Content":
                self.content = element[1].strip()
            elif element[0]=="Signature":
                self.signer = element[1].strip()
                self.signature = element[2].strip()
        file.close()
        # Check file
        if (self.EONclass != self.EONclass_):
            print(self.filename+" has class "+self.EONclass+" but "+self.EONclass_+" is needed!")
            return None
        if (self.EONversion != self.EONversion_):
            print(self.filename+" has version "+self.EONversion+" but "+self.EONversion_+" is needed!")
            return None
        # Check signature
        if self.checkSig():
            return self
        else:
            return None

    def create(self, content):
        self.EONclass = self.EONclass_
        self.EONversion = self.EONversion_
        self.content = content
        self.signer = self.identity
        self.signature = self.sign()
        # Create string
        message = self.message(True);
        # Calculate hash
        hash = str(self.hash());
        # Save to file
        self.filename = hash+".obj"
        file = open(self.doc_path+"/"+self.filename,"w+")
        file.write(message)
        file.close()
        return self;
