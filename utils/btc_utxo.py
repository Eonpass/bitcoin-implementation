#!usr/bin/python
import configparser
import json
from bitcoin_rpc_class import RPCHost
import datetime

config_file = "../eonpass_brand.conf"

config = configparser.RawConfigParser()
config.read(config_file)
rpcHost = config.get('BTC', 'host')
rpcPort = config.get('BTC', 'port')
rpcUser = config.get('BTC', 'username')
rpcPassword = config.get('BTC', 'password')
serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@'+rpcHost+':' + str(rpcPort)

print(serverURL)
host = RPCHost(serverURL)

def get_utxo():
    transactions = host.call('listunspent')
    for utxo in transactions:
        my_utxo = ""
        if utxo['spendable']:
            my_utxo = utxo['txid']
            my_vout = utxo['vout']
            my_addr = utxo['address']
            my_amount = utxo['amount']
            print('--[Available UTXO]--')
            print('\tmy utxo: '+ my_utxo)
            print('\tmy vout: '+str(my_vout))
            print('\tmy addr: '+ my_addr)
            print('\tamount: '+str(my_amount))
            print('--------------------')

get_utxo()
