########::'#######::'##::: ##:'########:::::'###:::::'######:::'######::
##.....::'##.... ##: ###:: ##: ##.... ##:::'## ##:::'##... ##:'##... ##:
##::::::: ##:::: ##: ####: ##: ##:::: ##::'##:. ##:: ##:::..:: ##:::..::
######::: ##:::: ##: ## ## ##: ########::'##:::. ##:. ######::. ######::
##...:::: ##:::: ##: ##. ####: ##.....::: #########::..... ##::..... ##:
##::::::: ##:::: ##: ##:. ###: ##:::::::: ##.... ##:'##::: ##:'##::: ##:
########:. #######:: ##::. ##: ##:::::::: ##:::: ##:. ######::. ######::
#.......:::.......:::..::::..::..:::::::::..:::::..:::......::::......:::
# EONPASS protocol - producer usage example
#    MIT License - Cryptomice 2018

import configparser
from eonpass.object import Object
from eonpass.contract import Contract
from eonpass.utxo import Utxo
from eonpass.prepoc import PrePoc
from eonpass.poc import Poc
from eonpass.transaction import Transaction
from eonpass.bitcoin_rpc_class import RPCHost
from shutil import copyfile, move, copy2 #, copytree, ignore_patterns
import time
import glob
import os
import random

DEBUG = True

print ("""
'########::'#######::'##::: ##:'########:::::'###:::::'######:::'######::
##.....::'##.... ##: ###:: ##: ##.... ##:::'## ##:::'##... ##:'##... ##:
##::::::: ##:::: ##: ####: ##: ##:::: ##::'##:. ##:: ##:::..:: ##:::..::
######::: ##:::: ##: ## ## ##: ########::'##:::. ##:. ######::. ######::
##...:::: ##:::: ##: ##. ####: ##.....::: #########::..... ##::..... ##:
##::::::: ##:::: ##: ##:. ###: ##:::::::: ##.... ##:'##::: ##:'##::: ##:
########:. #######:: ##::. ##: ##:::::::: ##:::: ##:. ######::. ######::
........:::.......:::..::::..::..:::::::::..:::::..:::......::::......:::
""")

config_file = "eonpass_logistic.conf"

def copytree(src, dst, symlinks=False, ignore=None):
   if not os.path.exists(dst):
       os.makedirs(dst)
   for item in os.listdir(src):
       s = os.path.join(src, item)
       d = os.path.join(dst, item)
       if os.path.isdir(s):
           #copytree(s, d, symlinks, ignore)
           avoid_dir=True
       else:
           if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
               copy2(s, d)

def getUtxo(chain):
   if not ((chain=='BTC') or (chain=='LTC')):
       print("invalid chain "+chain+"!")
       return None
   config = configparser.RawConfigParser()
   config.read(config_file)
   rpcHost = config.get(chain, 'host')
   rpcPort = config.get(chain, 'port')
   rpcUser = config.get(chain, 'username')
   rpcPassword = config.get(chain, 'password')
   serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@'+rpcHost+':' + str(rpcPort)
   host = RPCHost(serverURL)
   unspent = host.call('listunspent')

   utxo = []

   for transaction in unspent:
       tr = {}
       tr['chain']=chain
       tr['utxo']=transaction['txid']
       tr['vout']=transaction['vout']
       if(transaction['spendable']):
           utxo.append(tr)

   if (len(utxo)>0):
      id = random.randint(0, len(utxo)-1)
      return utxo[id]
   else:
      print("No UTXO available")
      return None

# wait for prepoc
print("............[LOGISTIC]...................................................")
print("Waiting for PrePOC")
found = False
while (not found):
   for file in glob.glob('./documents/logistic/incoming/'+'*.prepoc'):
       head, prepoc = os.path.split(file)
       found = True
       time.sleep(1)

files = os.listdir('./documents/logistic/incoming/')
for f in files:
       move('./documents/logistic/incoming/'+f, './documents/logistic/')
print("Found "+prepoc)

# create poc
print("............[LOGISTIC]...................................................")
print("Creating for POC")
tx = getUtxo('BTC')
nextSeal = Utxo(config_file).create(tx['chain'],tx['utxo'],tx['vout'],False)
if DEBUG:
   test = Utxo(config_file).load(nextSeal.filename)

poc = Poc(config_file).create(prepoc, nextSeal.filename)
if DEBUG:
   test = Poc(config_file).load(poc.filename)
print("Created "+nextSeal.filename)
print("Created "+poc.filename)

# send poc
print("............[LOGISTIC]...................................................")
print("Sending back POC")
copyfile("./documents/logistic/"+poc.filename, "./documents/production/incoming/"+poc.filename)

# wait for transaction
print("............[LOGISTIC]...................................................")
print("Waiting for transaction")
found = False
while (not found):
   for file in glob.glob('./documents/logistic/incoming/'+'*.transaction'):
       head, transaction = os.path.split(file)
       found = True
       time.sleep(1)

files = os.listdir('./documents/logistic/incoming/')
for f in files:
   move('./documents/logistic/incoming/'+f, './documents/logistic/')
print("Found "+transaction)

# wait for transaction spent
print("............[LOGISTIC]...................................................")
print("Waiting until transaction is confirmed")
tx = Transaction(config_file).load(transaction)
tx.waitUntilSpent(1)

print("............[LOGISTIC]..END..............................................")
