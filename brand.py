 ########::'#######::'##::: ##:'########:::::'###:::::'######:::'######::
 ##.....::'##.... ##: ###:: ##: ##.... ##:::'## ##:::'##... ##:'##... ##:
 ##::::::: ##:::: ##: ####: ##: ##:::: ##::'##:. ##:: ##:::..:: ##:::..::
 ######::: ##:::: ##: ## ## ##: ########::'##:::. ##:. ######::. ######::
 ##...:::: ##:::: ##: ##. ####: ##.....::: #########::..... ##::..... ##:
 ##::::::: ##:::: ##: ##:. ###: ##:::::::: ##.... ##:'##::: ##:'##::: ##:
 ########:. #######:: ##::. ##: ##:::::::: ##:::: ##:. ######::. ######::
#.......:::.......:::..::::..::..:::::::::..:::::..:::......::::......:::
# EONPASS protocol - brand usage example
#    MIT License - Cryptomice 2018

import configparser
from eonpass.object import Object
from eonpass.contract import Contract
from eonpass.utxo import Utxo
from eonpass.prepoc import PrePoc
from eonpass.poc import Poc
from eonpass.transaction import Transaction
from eonpass.bitcoin_rpc_class import RPCHost
from shutil import copyfile, move, copy2 #, copytree, ignore_patterns
import time
import glob
import os
import random

DEBUG = True

print ("""
'########::'#######::'##::: ##:'########:::::'###:::::'######:::'######::
 ##.....::'##.... ##: ###:: ##: ##.... ##:::'## ##:::'##... ##:'##... ##:
 ##::::::: ##:::: ##: ####: ##: ##:::: ##::'##:. ##:: ##:::..:: ##:::..::
 ######::: ##:::: ##: ## ## ##: ########::'##:::. ##:. ######::. ######::
 ##...:::: ##:::: ##: ##. ####: ##.....::: #########::..... ##::..... ##:
 ##::::::: ##:::: ##: ##:. ###: ##:::::::: ##.... ##:'##::: ##:'##::: ##:
 ########:. #######:: ##::. ##: ##:::::::: ##:::: ##:. ######::. ######::
........:::.......:::..::::..::..:::::::::..:::::..:::......::::......:::
""")

config_file = "eonpass_brand.conf"

def copytree(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            #copytree(s, d, symlinks, ignore)
            avoid_dir=True
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                copy2(s, d)

def getUtxo(chain):
    if not ((chain=='BTC') or (chain=='LTC')):
        print("invalid chain "+chain+"!")
        return None
    config = configparser.RawConfigParser()
    config.read(config_file)
    rpcHost = config.get(chain, 'host')
    rpcPort = config.get(chain, 'port')
    rpcUser = config.get(chain, 'username')
    rpcPassword = config.get(chain, 'password')
    serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@'+rpcHost+':' + str(rpcPort)
    host = RPCHost(serverURL)
    unspent = host.call('listunspent')

    utxo = []

    for transaction in unspent:
        tr = {}
        tr['chain']=chain
        tr['utxo']=transaction['txid']
        tr['vout']=transaction['vout']
        if(transaction['spendable']):
            if(transaction['amount'] >  0.00001000): #testnet
                utxo.append(tr)

    if (len(utxo)>0):
       id = random.randint(0, len(utxo)-1)
       return utxo[id]
    else:
       print("No UTXO available")
       return None

tx = getUtxo('LTC')
print("....[BRAND]..............................................................")
print("Creating generator")
generator = Utxo(config_file).create(tx['chain'],tx['utxo'],tx['vout'],True)
if DEBUG:
    test = Utxo(config_file).load(generator.filename)
print("Created "+generator.filename)

print("....[BRAND]..............................................................")
print("Creating object")
obj1 = Object(config_file).create("test1")
if DEBUG:
    test = Object(config_file).load(obj1.filename)
print("Created "+obj1.filename)

print("....[BRAND]..............................................................")
print("Creating object")
obj2 = Object(config_file).create("test2")
if DEBUG:
    test = Object(config_file).load(obj2.filename)
print("Created "+obj2.filename)

print("....[BRAND]..............................................................")
print("Creating object")
obj3 = Object(config_file).create("test3")
if DEBUG:
    test = Object(config_file).load(obj3.filename)
print("Created "+obj3.filename)

print("....[BRAND]..............................................................")
print("Creating contract")
list = []
list.append(obj1.filename)
list.append(obj2.filename)
list.append(obj3.filename)
contract = Contract(config_file).create(generator.filename,list)
if DEBUG:
    test = Contract(config_file).load(contract.filename)
print("Created "+contract.filename)

print("....[BRAND]..............................................................")
print("Creating PrePOC")
prePoc = PrePoc(config_file).create(contract.filename, generator.filename)
if DEBUG:
    test = PrePoc(config_file).load(prePoc.filename)
print("Created "+prePoc.filename)

print("....[BRAND]..............................................................")
print("Sending to production")
copytree("./documents/brand/", "./documents/production/incoming/", symlinks=False, ignore=None)

print("....[BRAND]..............................................................")
print("Waiting Poc")
# wait for poc
found = False
while (not found):
    for file in glob.glob('./documents/brand/incoming/'+'*.poc'):
        head, pocFilename = os.path.split(file)
        found = True
        time.sleep(1)

files = os.listdir('./documents/brand/incoming/')
for f in files:
    move('./documents/brand/incoming/'+f, './documents/brand/')
print("Found "+pocFilename)

# load poc
print("....[BRAND]..............................................................")
print("Loading POC")
poc = Poc(config_file).load(pocFilename)

print("....[BRAND]..............................................................")
print("Creating transaction")
# spend transaction
tx = Transaction(config_file).create(poc.filename)
tx.spend()
print("Created "+tx.filename)

print("....[BRAND]..............................................................")
print("Send transaction ")
# send transaction
copyfile("./documents/brand/"+tx.filename, "./documents/production/incoming/"+tx.filename)

print("....[BRAND]..END.........................................................")
