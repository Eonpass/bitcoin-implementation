# Eonpass Bitcoin/Litecoin implementation

This is a demonstration of the EONPASS protocol based on the concept of Single-Use Seal.

## Library

The Eonpass protocol library present in the folder _eonpass_ is developed for the language Python 3 and contain all the classes needed for the protocol.

## Example

The files brand.py, production.py and logistic.py shows how to use the protocol implementing the following steps:

1. Brand creates a Generator for the contract (on LTC)
2. Brand create Objects
3. Brand create Contract
4. Brand creates PrePOC and sends it to Production
5. Production wait for PrePOC
6. Production create a Next Seal (on BTC)
7. Production create a POC and send it back to Brand
8. Brand receive the POC and create the Transaction
9. Brand spend Transaction and communicate this action to Production (on LTC)
10. Production wait for the Transaction confirmation (on LTC)
11. Production creates PrePOC and sends it to Logistic
12. Logistic wait for PrePOC
13. Logistic create a Next Seal (on LTC)
14. Logistic create a POC and send it back to Production
15. Production receive the POC and create the Transaction
16. Production spend Transaction and communicate this action to Logistic (on BTC)
17. Logistic wait for the Transaction confirmation (on BTC)

The public/private keys used are available in the directory _keys_, all the documents generated are available in the folder _documents_.
