 ########::'#######::'##::: ##:'########:::::'###:::::'######:::'######::
 ##.....::'##.... ##: ###:: ##: ##.... ##:::'## ##:::'##... ##:'##... ##:
 ##::::::: ##:::: ##: ####: ##: ##:::: ##::'##:. ##:: ##:::..:: ##:::..::
 ######::: ##:::: ##: ## ## ##: ########::'##:::. ##:. ######::. ######::
 ##...:::: ##:::: ##: ##. ####: ##.....::: #########::..... ##::..... ##:
 ##::::::: ##:::: ##: ##:. ###: ##:::::::: ##.... ##:'##::: ##:'##::: ##:
 ########:. #######:: ##::. ##: ##:::::::: ##:::: ##:. ######::. ######::
#.......:::.......:::..::::..::..:::::::::..:::::..:::......::::......:::
# EONPASS protocol - producer usage example
#    MIT License - Cryptomice 2018

import configparser
from eonpass.object import Object
from eonpass.contract import Contract
from eonpass.utxo import Utxo
from eonpass.prepoc import PrePoc
from eonpass.poc import Poc
from eonpass.transaction import Transaction
from eonpass.bitcoin_rpc_class import RPCHost
from shutil import copyfile, move, copy2 #, copytree, ignore_patterns
import time
import glob
import os
import random

DEBUG = True

print ("""
'########::'#######::'##::: ##:'########:::::'###:::::'######:::'######::
 ##.....::'##.... ##: ###:: ##: ##.... ##:::'## ##:::'##... ##:'##... ##:
 ##::::::: ##:::: ##: ####: ##: ##:::: ##::'##:. ##:: ##:::..:: ##:::..::
 ######::: ##:::: ##: ## ## ##: ########::'##:::. ##:. ######::. ######::
 ##...:::: ##:::: ##: ##. ####: ##.....::: #########::..... ##::..... ##:
 ##::::::: ##:::: ##: ##:. ###: ##:::::::: ##.... ##:'##::: ##:'##::: ##:
 ########:. #######:: ##::. ##: ##:::::::: ##:::: ##:. ######::. ######::
........:::.......:::..::::..::..:::::::::..:::::..:::......::::......:::
""")

config_file = "eonpass_production.conf"

def copytree(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            #copytree(s, d, symlinks, ignore)
            avoid_dir=True
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                copy2(s, d)

def getUtxo(chain):
    if not ((chain=='BTC') or (chain=='LTC')):
        print("invalid chain "+chain+"!")
        return None
    config = configparser.RawConfigParser()
    config.read(config_file)
    rpcHost = config.get(chain, 'host')
    rpcPort = config.get(chain, 'port')
    rpcUser = config.get(chain, 'username')
    rpcPassword = config.get(chain, 'password')
    serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@'+rpcHost+':' + str(rpcPort)
    host = RPCHost(serverURL)
    unspent = host.call('listunspent')

    utxo = []

    for transaction in unspent:
        tr = {}
        tr['chain']=chain
        tr['utxo']=transaction['txid']
        tr['vout']=transaction['vout']
        if(transaction['spendable']):
            utxo.append(tr)

    if (len(utxo)>0):
       id = random.randint(0, len(utxo)-1)
       return utxo[id]
    else:
       print("No UTXO available")
       return None

# wait for prepoc
print("........[PRODUCTION].....................................................")
print("Waiting for PrePOC")
found = False
while (not found):
    for file in glob.glob('./documents/production/incoming/'+'*.prepoc'):
        head, prepoc = os.path.split(file)
        found = True
        time.sleep(1)

files = os.listdir('./documents/production/incoming/')
for f in files:
        move('./documents/production/incoming/'+f, './documents/production/')
print("Found "+prepoc)

# create poc
print("........[PRODUCTION].....................................................")
print("Creating for POC")
tx = getUtxo('BTC')
nextSeal = Utxo(config_file).create(tx['chain'],tx['utxo'],tx['vout'],False)
if DEBUG:
    test = Utxo(config_file).load(nextSeal.filename)

poc = Poc(config_file).create(prepoc, nextSeal.filename)
if DEBUG:
    test = Poc(config_file).load(poc.filename)
print("Created "+nextSeal.filename)
print("Created "+poc.filename)

# send poc
print("........[PRODUCTION].....................................................")
print("Sending back POC")
copyfile("./documents/production/"+poc.filename, "./documents/brand/incoming/"+poc.filename)

# wait for transaction
print("........[PRODUCTION].....................................................")
print("Waiting for transaction")
found = False
while (not found):
    for file in glob.glob('./documents/production/incoming/'+'*.transaction'):
        head, transaction = os.path.split(file)
        found = True
        time.sleep(1)

files = os.listdir('./documents/production/incoming/')
for f in files:
    move('./documents/production/incoming/'+f, './documents/production/')
print("Found "+transaction)

# wait for transaction spent
print("........[PRODUCTION].....................................................")
print("Waiting until transaction is confirmed")
tx = Transaction(config_file).load(transaction)
tx.waitUntilSpent(1)
### to logistic

# create prepoc
print("........[PRODUCTION].....................................................")
print("Creating PrePOC")
pocFilename = poc.contract+".contract"
myUtxoFilename = poc.nextChain+"_"+poc.nextUtxo+"_"+poc.nextVout+".utxo"
prePoc = PrePoc(config_file).create(pocFilename, myUtxoFilename)
if DEBUG:
    test = PrePoc(config_file).load(prePoc.filename)
print("Created "+prePoc.filename)

# send
print("........[PRODUCTION].....................................................")
print("Sending the PrePOC")
copytree("./documents/production/", "./documents/logistic/incoming/",  symlinks=False, ignore=None)

print("........[PRODUCTION].....................................................")
print("Waiting Poc")
# wait for poc
found = False
while (not found):
    for file in glob.glob('./documents/production/incoming/'+'*.poc'):
        head, pocFilename = os.path.split(file)
        found = True
        time.sleep(1)

files = os.listdir('./documents/production/incoming/')
for f in files:
    move('./documents/production/incoming/'+f, './documents/production/')
print("Found "+pocFilename)

# load poc
print("........[PRODUCTION].....................................................")
print("Loading POC")
poc = Poc(config_file).load(pocFilename)

print("........[PRODUCTION].....................................................")
print("Creating transaction")
# spend transaction
tx = Transaction(config_file).create(poc.filename)
tx.spend()
print("Created "+tx.filename)

print("........[PRODUCTION].....................................................")
print("Send transaction ")
# send transaction
copyfile("./documents/production/"+tx.filename, "./documents/logistic/incoming/"+tx.filename)

print("........[PRODUCTION]..END................................................")
